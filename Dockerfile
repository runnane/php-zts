FROM php:8.2.12-zts-bullseye

# Extensions
RUN apt-get update -yqq
RUN apt-get install -yqq libssh2-1-dev libssh2-1 ca-certificates iputils-ping \
  mariadb-client gnupg git curl libonig-dev libcurl4-gnutls-dev libzip-dev \
  zip unzip libicu-dev libmcrypt-dev libvpx-dev libxpm-dev \
  zlib1g-dev libxml2-dev libexpat1-dev libbz2-dev libgmp3-dev \
  libldap2-dev unixodbc-dev libpq-dev libsqlite3-dev libaspell-dev libsnmp-dev \
  libpcre3-dev libtidy-dev libsasl2-dev libssl-dev gnupg2 libldb-dev 

RUN mkdir -p /etc/apt/keyrings
RUN curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg
RUN echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_20.x nodistro main" | tee /etc/apt/sources.list.d/nodesource.list
RUN apt-get update -yqq
RUN apt-get install nodejs -yqq

RUN pecl channel-update pecl.php.net
RUN pecl install parallel

RUN docker-php-ext-install -j$(nproc) iconv bcmath mysqli pcntl pdo_mysql 
RUN docker-php-ext-enable parallel 

# Composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

VOLUME /srv
WORKDIR /srv